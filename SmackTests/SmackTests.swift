//
//  SmackTests.swift
//  SmackTests
//
//  Created by prasad-shinde on 18/07/19.
//  Copyright © 2019 Prasad. All rights reserved.
//

import XCTest
@testable import Smack

class SmackTests: XCTestCase {

    var sut: CreateAccountVC!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "CreateAccountVC") as? CreateAccountVC
        _ = sut.view
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        sut = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPassFieldClearedOnLoginError() {
        sut.passTxtField.text = "abc"
        sut.validateAndLogin()
        XCTAssertEqual(sut.passTxtField.text?.count, 0, "Password field not cleared")
    }
}
