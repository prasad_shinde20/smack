//
//  LoginVC.swift
//  Smack
//
//  Created by Prasad on 05/07/18.
//  Copyright © 2018 Prasad. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func closeBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func createAccBtnPressed (_ sender : UIButton){
        performSegue(withIdentifier: TO_CREATE_ACCOUNT, sender: nil)
    }
    

}
