//
//  ChatVC.swift
//  Smack
//
//  Created by Prasad on 05/07/18.
//  Copyright © 2018 Prasad. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {

   
    
    @IBOutlet weak var menuBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        menuBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
        
    }

    

}
//test
