//
//  CreateAccountVC.swift
//  Smack
//
//  Created by Prasad on 05/07/18.
//  Copyright © 2018 Prasad. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {

    @IBOutlet weak var usernameTxtField: UITextField!
    @IBOutlet weak var passTxtField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func closePressed(_ sender: UIButton) {
        performSegue(withIdentifier: UNWIND, sender: nil)
    }
    
    @IBAction func loginPressed(_ sender: UIButton) {
        validateAndLogin()
    }
    
    func validateAndLogin() {
        if let pass = passTxtField.text,
            pass.count > 8 {
            print("ok")
        } else {
            usernameTxtField.text = ""
            passTxtField.text = ""
        }
    }
}
