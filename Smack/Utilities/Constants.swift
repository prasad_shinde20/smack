//
//  Constants.swift
//  Smack
//
//  Created by Prasad on 05/07/18.
//  Copyright © 2018 Prasad. All rights reserved.
//

import Foundation

//Segues
let TO_LOGIN = "toLogin"
let TO_CREATE_ACCOUNT = "toCreateAccount"
let UNWIND = "unwindToChannel"
