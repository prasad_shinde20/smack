
#!/usr/bin/env bash

GIT_DIR=$(git rev-parse --git-dir)
CURR_DIR=$(dirname $PWD/$0)

rm -rf $GIT_DIR/hooks/pre-push

echo "Installing hooks..."

# this command creates symlink to our pre-commit script
ln -s $CURR_DIR/pre-push.sh $GIT_DIR/hooks/pre-push

echo "Completed!"
