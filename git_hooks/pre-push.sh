
#!/usr/bin/env bash

echo "Running pre-push hook"

xcodebuild test -workspace Smack.xcworkspace -scheme Smack -destination 'platform=iOS Simulator,OS=12.2,name=iPhone 7' CODE_SIGN_IDENTITY='' CODE_SIGNING_REQUIRED=NO

# $? stores exit value of the last command
if [ $? -ne 0 ]; then
echo "Tests must pass before commit!"
exit 1
fi
